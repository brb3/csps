# Chattanooga Security and Privacy Society

## Signing the roster

A list of attendees is created for each meeting. If someone does not wish to be on the list they will be put on the list as 'anon.' The lists can be found in the `source/meetings` directory. We encourage people to sign the roster with their PGP key if they would like. 

### How to sign the roster.

1. Fork the CSPS repo
2. Check out the branch created for that night's meeting
3. Sign with: `gpg --armor --detach-sig 1970-01-01-csps`
4. Append your name to the signature filename: `1970-01-01-csps-john-smith`
5. Commit your changes
6. Push to your fork
7. Create a merge request for the project repo

If you have any issues with this don't hesitate to ask for help!
 